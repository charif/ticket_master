create database ticket_master;
use ticket_master;
create table SPECTACLE
 (
   id int primary key auto_increment,
	nomSpectacle varchar(30),
    description varchar(150),
    dateDebut DATE,
    dateFin DATE,
    image varchar(50)
   	);
create table REPRESENTATION
 (
    id int primary key auto_increment,
    id_spectacle int,
	nomRepresentation varchar(30),
    description varchar(150),
    dateRepresentation DATE,
    heureDebut time,
    heureFin time,
    nombrePlaces int,
    tarif decimal(10,2),
    image varchar(50),
     CONSTRAINT fk_id_spectacle      -- On donne un nom à notre clé
    FOREIGN KEY (id_spectacle)   -- Colonne sur laquelle on crée la clé
    REFERENCES SPECTACLE(id)     -- Colonne de référence (celle de la table Client)
    ON DELETE SET NULL        -- Action à effectuer lors de la suppression d'une référence 
   	);