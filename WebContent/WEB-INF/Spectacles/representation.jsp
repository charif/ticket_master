<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- <title>Basic Company Template for Bootstrap 3</title> -->

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS for the 'Heroic Features' Template -->
    <link href="css/heroic-features.css" rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value="/acceuil"/>">Accueil</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling 	-->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#contact">Contact</a>
                    </li>
					<li><a href="#panier">Panier</a> 
                    </li>
                </ul>
            </div>
		
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">
	
	  <div class="jumbotron hero-spacer">
            <h1>Ticket-Master !!!</h1>
      </div>
	
	

   <form method="get" action ="<c:url value ="/representation"/>" >
       <table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th></th>
					<th>Spectacle</th>
					<th>Ville</th>
					<th>Date et Heure</th>
					<th>Prix</th>
					<th>Places disponibles</th>
					<th>Quantite</th>
				</tr>
			</thead>
			
	
		<tbody>
			 <c:forEach items ="${representations}" var ="listeRepresentation" varStatus ="boucle">
					<tr>
					
						<td><img src="${listeRepresentation.image}" alt=""  height="80" width="120"></td>
						<td><strong><c:out value="${listeRepresentation.nomRepresentation}"/></strong></td>
						<td><c:out value="${listeRepresentation.description}"/></td>
						<td><c:out value="${listeRepresentation.dateRepresentation}"/></td>
						<td><c:out value="${listeRepresentation.tarif}"/></td>
						<td><c:out value="${listeRepresentation.nombrePlaces}"/></td>
						<td><SELECT>
			                <OPTION>0</OPTION>
			                <OPTION>1</OPTION>
			                <OPTION>2</OPTION>
			                <OPTION>3</OPTION>
			                <OPTION>4</OPTION>
			                <OPTION>5</OPTION>
			                <OPTION>6</OPTION>
							</SELECT></td>
					</tr>
		      </c:forEach>				
			</tbody>
	   </table>
	 </form>
	   
	   <p class="text-right"><button type="button" class="btn btn-primary">AJOUTER AU PANIER</button></p>
	   <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Company 2014</p>
                </div>
            </div>
        </footer>

    </div>
	
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

</body>

</html>
