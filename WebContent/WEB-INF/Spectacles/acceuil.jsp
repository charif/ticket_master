<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <title>Basic Company Template for Bootstrap 3</title> -->

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS for the 'Heroic Features' Template -->
    <link href="css/heroic-features.css" rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="<c:url value="/acceuil"/>">Accueil</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling 	-->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#contact">Contact</a>
                    </li>
					<li><a href="#panier">Panier</a> 
                    </li>
                </ul>
            </div>
		
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">

        <div class="jumbotron hero-spacer" style="background-image: url('image/theatre.jpg'); color:white;">
            <h1>Ticket-Master !!!</h1>
            <p >Découvrez les meilleurs endroits pour assister à  vos spectacles préférés, avec les prix les moins chers en ville!</p>
            <p><a class="btn btn-primary btn-large">Calendrier</a>
            </p>
        </div>
        <form method="get" action="<c:url value="/rechercher" />">
       			<input type ="text" id="txt" name="txt"  class="text"> 
			<input type="submit" id = "Rechercher" value="Rechercher" class ="btn btn-primary">
		</form>   

        <hr>

        <div class="row">
            <div class="col-lg-12">
                <h3>Latest Features</h3>
            </div>
        </div>
        <!-- /.row -->
        <form method="get" action ="<c:url value ="/acceuil"/>" >

        <div class="row text-center">
             <c:forEach items ="${spectacles}" var ="listeSpectacle" varStatus ="boucle">
           
		            <div class="col-lg-3 col-md-6 hero-feature">
		                <div class="thumbnail">
		                    <img src="${listeSpectacle.image}"  alt="">
		                    <div class="caption">
		                        <h3><c:out value="${listeSpectacle.nomSpectacle}"/></h3>
		                        <p><c:out value="${listeSpectacle.description}"/></p>

			                       <p><a href="<c:url value="/representation"><c:param name="idSpectacle" value="${ listeSpectacle.id }" /></c:url>"  class="btn btn-primary">Plus d'nfo!</a>
		                       
		                        <!--<p><a href="<c:url value="/representation" />" class="btn btn-primary">Plus d'nfo!</a>  <!--<a href="#" class="btn btn-default">More Info</a> -->
		                        </p>
		                    </div>
		                </div>
		            </div>
		      </c:forEach>

            
        </div>
        </form>
        <!-- /.row -->

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Company 2014</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

</body>

</html>
