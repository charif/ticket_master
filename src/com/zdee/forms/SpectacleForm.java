package com.zdee.forms;

import java.sql.Timestamp;
import java.util.ArrayList;

import com.zdee.beans.Spectacle;
import com.zdee.dao.SpectacleDAO;

public final class SpectacleForm {
	
	private static final String ID = "id" ;
	private static final  String NOM_SPECTACLE ="nomSpectacle";
    private static final String DESCRIPTION = "description"; 
    private static final String DATEDEBUT = "dateDebut";
    private static final String DATEFIN = "dateFin";
    private static final String IMAGE = "image";
    private SpectacleDAO  spectacleDAO;
    
   public SpectacleForm(SpectacleDAO spectacleDAO){
	   this.spectacleDAO = spectacleDAO;
	   
   }
    
    public ArrayList<Spectacle> Lister(){
    	return spectacleDAO.Lister();
    	
    }
	

}
 