package com.zdee.beans;

import java.sql.Timestamp;

public class Representation {
	
	private int id ;
	private int id_spectacle;
 	private String nomRepresentation;
    private String description;
    private Timestamp dateRepresentation;
    private Timestamp heureDebut ;
    private Timestamp heureFin;
    private int nombrePlaces;
    private double tarif;
    private String image;
    private Spectacle spectacle;
    
    
	public int getId_spectacle() {
		return id_spectacle;
	}
	public void setId_spectacle(int id_spectacle) {
		this.id_spectacle = id_spectacle;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomRepresentation() {
		return nomRepresentation;
	}
	public void setNomRepresentation(String nomRepresentation) {
		this.nomRepresentation = nomRepresentation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Timestamp getDateRepresentation() {
		return dateRepresentation;
	}
	public void setDateRepresentation(Timestamp dateRepresentation) {
		this.dateRepresentation = dateRepresentation;
	}
	public Timestamp getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(Timestamp heureDebut) {
		this.heureDebut = heureDebut;
	}
	public Timestamp getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(Timestamp heureFin) {
		this.heureFin = heureFin;
	}
	public int getNombrePlaces() {
		return nombrePlaces;
	}
	public void setNombrePlaces(int nombrePlaces) {
		this.nombrePlaces = nombrePlaces;
	}
	public double getTarif() {
		return tarif;
	}
	public void setTarif(double tarif) {
		this.tarif = tarif;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Spectacle getSpectacle() {
		return spectacle;
	}
	public void setSpectacle(Spectacle spectacle) {
		this.spectacle = spectacle;
	}
    

}
