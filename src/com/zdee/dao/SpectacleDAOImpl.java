package com.zdee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import static com.zdee.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.zdee.dao.DAOUtilitaire.initialisationRequetePreparee;

import com.zdee.beans.Spectacle;

public class SpectacleDAOImpl implements SpectacleDAO{
	private int id;
	private String nomSpectacle;
    private String description;
    private Timestamp  dateDebut;
    private Timestamp dateFin;
    private String image;
	
	private static final String  SQL_SELECT ="SELECT  id,nomSpectacle,description,dateDebut,dateFin,image  FROM SPECTACLE";
	private DAOFactory daofactory;

	SpectacleDAOImpl (DAOFactory daofactory){
		this.daofactory=daofactory;
		
	}
	@Override
	public ArrayList<Spectacle> Lister() {
		ArrayList<Spectacle> listSpectacles = new ArrayList<Spectacle>();
	    Connection connexion =null;
	    PreparedStatement preparestatement = null;
	    ResultSet resultset = null;
	    try{
	    	connexion = daofactory.getConnection();
	    	preparestatement = connexion.prepareStatement(SQL_SELECT);//initialisationRequetePreparee(connexion,SQL_SELECT,false);
	    	resultset  =  preparestatement.executeQuery();
	    	while(resultset.next()){
	    		listSpectacles.add(map(resultset));
	    		
	    	}
	    }catch(SQLException e){
	    	throw new DAOException(e);
	    	
	    }finally{
	    	
	    	fermeturesSilencieuses(resultset,preparestatement,connexion);
	    }
	    
	    
		return listSpectacles;
	}
	
	private static Spectacle map(ResultSet resultset) throws SQLException
	{
		Spectacle spectacle = new Spectacle();
		spectacle.setId(resultset.getInt("id"));
		spectacle.setNomSpectacle(resultset.getString("nomSpectacle"));
		spectacle.setDateDebut(resultset.getTimestamp("dateDebut"));
		spectacle.setDateFin(resultset.getTimestamp("dateFin"));
		spectacle.setImage(resultset.getString("image"));
		spectacle.setDescription(resultset.getString("description"));
		
		
		return spectacle;
		
	}

}
