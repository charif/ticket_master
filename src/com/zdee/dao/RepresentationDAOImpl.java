package com.zdee.dao;

import static com.zdee.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.zdee.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.zdee.beans.Representation;

public class RepresentationDAOImpl implements RepresentationDAO  {
	
	private int id;
	private int id_spectacle;
	private String nomRepresentation;
    private String description;
    private Timestamp  dateRepresentation;
    private Timestamp heureDebut;
    private Timestamp heureFin;
    private int nombrePlaces;
    private double tarif;
    private String image;
    
  	
    private static final String  SQL_SELECT ="SELECT  id,id_spectacle,nomRepresentation,description,dateRepresentation,heureDebut,heureFin,nombrePlaces,tarif,image  FROM REPRESENTATION WHERE id_spectacle =?";
	private DAOFactory daofactory;

	RepresentationDAOImpl (DAOFactory daofactory){
		this.daofactory=daofactory;
	}
	
	public ArrayList<Representation> Lister(int id_spectacle){
		ArrayList<Representation> listRepresentations = new ArrayList<Representation>();
	    Connection connexion =null;
	    PreparedStatement preparestatement = null;
	    ResultSet resultset = null;
	    try{
	    	connexion = daofactory.getConnection();
	    	preparestatement = initialisationRequetePreparee(connexion,SQL_SELECT,false,id_spectacle);
	    	resultset  =  preparestatement.executeQuery();
	    	while(resultset.next()){
	    		listRepresentations.add(map(resultset));
	    		
	    	}
	    }catch(SQLException e){
	    	throw new DAOException(e);
	    	
	    }finally{
	    	
	    	fermeturesSilencieuses(resultset,preparestatement,connexion);
	    }
	    
	    
		return listRepresentations;
	}
	
	private static Representation map(ResultSet resultset) throws SQLException
	{
		Representation Representation = new Representation();
		Representation.setId(resultset.getInt("id"));
		Representation.setId_spectacle(resultset.getInt("id_spectacle"));
		Representation.setDescription(resultset.getString("description"));
		Representation.setNomRepresentation(resultset.getString("nomRepresentation"));
		Representation.setDateRepresentation(resultset.getTimestamp("dateRepresentation"));
		Representation.setHeureDebut(resultset.getTimestamp("heureDebut"));
		Representation.setHeureDebut(resultset.getTimestamp("heureFin"));
		Representation.setNombrePlaces(resultset.getInt("nombrePlaces"));
		Representation.setTarif(resultset.getDouble("tarif"));
		Representation.setImage(resultset.getString("image"));
		
	return Representation;
		
	}


}
