package com.zdee.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zdee.beans.Representation;
import com.zdee.dao.DAOFactory;
import com.zdee.dao.RepresentationDAO;
import com.zdee.forms.RepresentationForm;


@WebServlet( "/representation" )
public class ListerRepresentation extends HttpServlet {
	
	private RepresentationDAO     representationDAO;
	private static final long serialVersionUID = 1L;
	public static final String CONF_DAO_FACTORY = "daofactory";
    public static final String ATT_REPRESENTATION         = "representations";
    public static final String ATT_FORM         = "form";
    public static final String VUE              = "/WEB-INF/Spectacles/representation.jsp";
    
  
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.representationDAO = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getRepresentationDAO();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page d'inscription */
    	RepresentationForm Representationform = new RepresentationForm(representationDAO);
    	ArrayList<Representation> listRepresentations = Representationform.Lister(Integer.parseInt(request.getParameter("idSpectacle")));
    	request.setAttribute(ATT_REPRESENTATION, listRepresentations);
    	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    	
    }

}
