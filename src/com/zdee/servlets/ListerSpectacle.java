package com.zdee.servlets;

import java.io.IOException;

import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zdee.beans.Spectacle;
import com.zdee.dao.DAOFactory;
import com.zdee.dao.SpectacleDAO;
import com.zdee.forms.SpectacleForm;

@WebServlet( "/acceuil" )
public class ListerSpectacle extends HttpServlet {
	
	private SpectacleDAO     spectacledao;
	private static final long serialVersionUID = 1L;
	public static final String CONF_DAO_FACTORY = "daofactory";
    public static final String ATT_SPECTACLE         = "spectacles";
    public static final String ATT_FORM         = "form";
    public static final String VUE              = "/WEB-INF/Spectacles/acceuil.jsp";
    
  
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.spectacledao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getSpectacleDAO();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page d'inscription */
    	SpectacleForm spectacleform = new SpectacleForm(spectacledao);
    	ArrayList<Spectacle> listSpectacles = spectacleform.Lister();
    	request.setAttribute(ATT_SPECTACLE, listSpectacles);
    	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    	
    }

}
